<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Tarif;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Shop
 *
 * @ORM\Table(name="shop")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShopRepository")
 */
class Shop
{
    /**
     * Unikatni ID shopu
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    protected $id;

    /**
     * Název eshopoveho řešení
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    protected $name;

    /**
     * Popis eshopového řešení
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     * @var text
     */
    protected $text;
    /**
     * Tarify eshopového řešení
     * ORM\OneToMany(targetEntity="Tarif", mappedBy="shop")
     * @var text
     */
    protected $tarifs;


    function __construct() {
        $this->tarifs = new ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getText() {
        return $this->text;
    }
    function getTarifs() {
        return $this->tarifs;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setText($text) {
        $this->text = $text;
    }
    

    function setTarifs($tarifs) {
        $this->tarifs = $tarifs;
    }

    function addTarifs($tarif) {
        $this->tarifs[] = $tarif;
    }

    function __toString() {
        return $this->name;
    }
}
