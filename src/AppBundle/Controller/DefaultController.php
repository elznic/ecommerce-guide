<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AppBundle\Entity\ShopTarif;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', array(
            //'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            
        ));
    }
    /**
     * @Route("/vyber", name="choose")
     */
    public function chooseAction(Request $request)
    {
        $defaultData = array('message' => 'Type your message here');
        //Formulář nemá vlastní Type, protože se neukládá do databáze
        $form = $this->createFormBuilder($defaultData)
            ->add('products', NumberType::class, array(
                'label'    => 'Kolik budete mít v obchodě produktů?',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Počet produktů' ),
                //'label_attr' => array('class' => 'control-label col-md-8' ),
                'required' => true,
            ))
            ->add('store', CheckboxType::class, array(
                'attr' => array('class' => 'form-control', 'placeholder' => 'Počet produktů' ),
                'label'    => 'Budete potřebovat aplikaci pro pokladnu?',
                'required' => false,
            ))
            ->add('shipping', CheckboxType::class, array(
                'label'    => 'Budete potřebovat export pro České dopravce?',
                'required' => false,
            ))
            ->add('filtrs', CheckboxType::class, array(
                'label'    => 'Budete potřebovat filtrovat produkty dle parametrů?',
                'required' => false,
            ))
            ->add('ratings', CheckboxType::class, array(
                'label'    => 'Budete chtít nechat zákazníky hodnotit zboží?',
                'required' => false,
            ))
            ->add('related', CheckboxType::class, array(
                'label'    => 'Budete chtít zobrazovat podobné zboží?',
                'required' => false,
            ))
            ->add('action', CheckboxType::class, array(
                'label'    => 'Budete chtít nabízet akční zboží?',
                'required' => false,
            ))
            ->add('stock', CheckboxType::class, array(
                'label'    => 'Budete potřebovat vést skaldové hospodářství?',
                'required' => false,
            ))
            ->add('virtual', CheckboxType::class, array(
                'label'    => 'Budete prodávat virtuální zboží (ebooky, hry)?',
                'required' => false,
            ))
            ->add('wholesale', CheckboxType::class, array(
                'label'    => 'Budete chtít velkoobchodní prodej?',
                'required' => false,
            ))
            //Platby
            ->add('invoiceSystem', CheckboxType::class, array(
                'label'    => 'Budete potřebovat napojení na ERP (např. Abra nebo Lekis)?',
                'required' => false,
            ))
            //->add('invoiceSystem', EntityType::class, array(
            //    'class' => 'AppBundle:InvoiceSystem',
            //    'choice_label' => 'name',
            //))
            ->add('onlinePayment', CheckboxType::class, array(
                'label'    => 'Budete chtít platbu kartou?',
                'required' => false,
            ))
            ->add('bank', CheckboxType::class, array(
                'label'    => 'Budete chtít automaticky párovat platby z banky?',
                'required' => false,
            ))
            ->add('currencies', CheckboxType::class, array(
                'label'    => 'Budete chtít prodávat v cizích měnách?',
                'required' => false,
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            // data is an array with "name", "email", and "message" keys
            $em = $this->getDoctrine()->getManager();
            $tarifs = $em->getRepository('AppBundle:Tarif')->findAllByValues($data);
            if(count($tarifs) == 0){
                return $this->render('default/noresult.html.twig');
            }
            
            //Get shop associated with this tarif
            $shops = array();
            //counter for firt three
            $i = 0;
            foreach ($tarifs as $key => $value) {
                $i++;
                $shop = $em->getRepository('AppBundle:Shop')->findBy(array('id' => $value->getShop()));
                array_push($shops,new ShopTarif($shop[0],$value));
                if($i >= 3){break;}
            }
            return $this->render('default/result.html.twig', array(
                'shops'     => $shops
                ));
        }

        // ... render the form

        return $this->render('default/choose.html.twig', array(
            //'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'form' => $form->createView(),
        ));
    }

}
