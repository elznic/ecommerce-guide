<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Shop;
use AppBundle\Form\ShopType;
use AppBundle\Security\Voter\ShopVoter;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Shop controller.
 *
 * @Route("shop")
 */
class ShopController extends Controller
{
    /**
     * Lists all shop entities.
     *
     * @Route("/", name="shop_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page');
        if(empty($page)){
            $page = 1;
        }
        $max = 5;

        $em = $this->getDoctrine()->getManager();

        $shops = $em->getRepository('AppBundle:Shop')->findAllByPage($page, $max);
        
        $total = $shops->count();
        $maxPages = ceil($shops->count() / $max);
        $currPage = $page;
        
        return $this->render('shop/index.html.twig', array(
            'shops' => $shops,
            'total' => $total,
            'maxPages' => $maxPages,
            'currPage' => $currPage,
        ));
    }

    /**
     * @Route("/new", name="shop_new")
     */
    public function newAction(Request $request)
    {
        $shop = new Shop();
        $this->denyAccessUnlessGranted(ShopVoter::EDIT, $shop);
        $form = $this->createForm(ShopType::class, $shop);

        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $shop = $em->getRepository('AppBundle:Shop')->createShop($shop);
                return $this->redirectToRoute('shop_show', array('name' => $shop->getName( ) ) );
            }
        }

        return $this->render('shop/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/edit/{id}", name="shop_edit")
     */
    public function editAction(Request $request, $id){
        //$request = $this->get('data_collector.request');
        $em = $this->getDoctrine()->getManager();
        $shop = $em->getRepository('AppBundle:Shop')->findById($id);
        $this->denyAccessUnlessGranted(ShopVoter::EDIT, $shop);

        $form = $this->createForm(ShopType::class, $shop);

        $form->handleRequest($request);

        if($form->isSubmitted()) {
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $shop = $em->getRepository('AppBundle:Shop')->createShop($shop);
                return $this->redirectToRoute('shop_show', array('name' => $shop->getName( ) ) );
            }
        }

        return $this->render('shop/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/delete/{id}", name="shop_delete")
     */
    public function deleteAction(Request $request, Shop $shop)
    {
        $this->denyAccessUnlessGranted(ShopVoter::DELETE, $shop);
        $em = $this->getDoctrine()->getManager();
        $tarifs = $em->getRepository('AppBundle:Tarif')->findBy(array('shop' => $shop));
        foreach ($tarifs as $key => $value) {
            $em->getRepository('AppBundle:Tarif')->delete($value);
        }
        $em->getRepository('AppBundle:Shop')->delete($shop);
        return $this->redirectToRoute('shop_index');
    }


    /**
     * Finds and displays a shop entity.
     *
     * @Route("/{name}", name="shop_show")
     * @Method("GET")
     */
    public function showAction(Shop $shop)
    {
        $em = $this->getDoctrine()->getManager();
        $tarifs = $em->getRepository('AppBundle:Tarif')->findBy(array('shop' => $shop));
        return $this->render('shop/show.html.twig', array(
            'shop' => $shop,
            'tarifs' => $tarifs,
        ));
    }
}
